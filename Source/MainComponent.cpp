/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    /** Set Button 1's attributes */
    textButton1.setButtonText("*Plugin Button*");
    addAndMakeVisible(textButton1);
    textButton1.addListener(this);
    
    textButton2.setButtonText("*2nd Plugin Button*");
    addAndMakeVisible(textButton2);
    textButton2.addListener(this);
    
    slider1.setSliderStyle(Slider::LinearBar);
    addAndMakeVisible(slider1);
    slider1.addListener(this);
    
    textEditor1.setText("Write Here");
    addAndMakeVisible(textEditor1);
    
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    textButton1.setBounds(10, 10, getWidth() - 20, 40);
    
    slider1.setBounds(10, 50, getWidth() - 20, 40);
    
    textEditor1.setBounds(10, 130, getWidth() - 20, 40);
    
    textButton2.setBounds(10, 210, getWidth() - 20, 40);
    
    DBG("Wid: " << getWidth() << "\nHei: " << getHeight() << "\n" );
}

void MainComponent::paint(Graphics& g)
{
    g.fillAll(Colours::turquoise);
    
}

void MainComponent::buttonClicked(Button* button)
{
    if(button == &textButton1)
    {
    DBG ("Button 1 Clicked\n");
    }
    else if(button == &textButton2)
    {
        DBG ("Button 2 Clicked\n");
    }
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    DBG ("Slider val: " << slider1.getValue() << "\n");
}
